import { check } from 'k6';
import http from 'k6/http';

export const test2Prices=()=>{
  const res = http.get('https://test-api.k6.io');
  check(res, {
    'test 2 Prices': () => res.status === 200,
  });
}
