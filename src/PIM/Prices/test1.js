import { check } from 'k6';
import http from 'k6/http';

export const test1Prices=()=>{
  const res = http.get('https://test-api.k6.io');
  check(res, {
    'test 1 Prices': () => res.status === 200,
  });
}
