import {test1} from './Products/test1.js'
import {test2} from './Products/test2.js'
import {test1Prices} from './Prices/test1.js'
import {test2Prices} from './Prices/test2.js'

export let options = {
  vus: 10,
  duration: '10s'
};

export default () => {
  test1(); 
  test2();
  test1Prices();
  test2Prices();
};