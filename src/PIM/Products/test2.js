import { check } from 'k6';
import http from 'k6/http';

export const test2=()=>{
  const res = http.get('https://test-api.k6.io');
  check(res, {
    'test 2 Product': () => res.status === 200,
  });
}
