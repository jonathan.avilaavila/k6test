import {test1} from './Products/test1.js'
import {test2} from './Products/test2.js'

export let options = {
  vus: 10,
  duration: '10s'
};

export default () => {
  test1(); 
  test2();
};