import { check } from 'k6';
import http from 'k6/http';

export const test1=()=>{
  const res = http.get('https://test-api.k6.io');
  check(res, {
    'test 1 products': () => res.status === 200,
  });
}
